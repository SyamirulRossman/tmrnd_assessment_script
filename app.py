#import libraries
from flask import Flask, render_template
from flask_restful import Resource, Api, reqparse
import pandas as pd
import sqlite3

import threading
import csv_loader

#init flask
app = Flask(__name__)
api = Api(app)

def listenCSV():
    csv_loader.listenCSV()

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/assignment_result',methods=['POST'])
def post():
    parser = reqparse.RequestParser()  # initialize
    
    parser.add_argument('name', required=True)
    parser.add_argument('team_ID', required=True)
    
    args = parser.parse_args()  # parse arguments to dictionary

    print ("Received input from client, Name: " + args['name'] +" TeamID: " + args['team_ID'] + " matching inputs with JSON")

    with open("activity_log.log", "a+") as file_object:
        file_object.seek(0)
        data = file_object.read(100)
        if len(data) > 0 :
            file_object.write("\n")
        file_object.write("Received input from client, Name: " + args['name'] +" TeamID: " + args['team_ID'] + " matching inputs with JSON")
    
    # create new dataframe containing new values
    new_data = pd.DataFrame({
        "NAME": args['name'],
        "TEAM_ID": args['team_ID']
    }, index=[0])
    # read JSON
    data = pd.read_json('assignment_result.json')

    result = pd.merge(new_data,data,on='TEAM_ID',how='inner')

    #insert
    connection = sqlite3.connect("tmrnd.db")

    cursor=connection.cursor()

    result.to_sql('assignment_result', connection, if_exists='append', index = False)

    cursor.close()
    connection.close()

    return render_template('result.html',  tables=[result.to_html(classes='data')], titles=result.columns.values)


if __name__ == '__main__':
    t1 = threading.Thread(target=listenCSV)
    t1.start()

    app.run()