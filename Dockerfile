FROM python:3.9-slim-buster

WORKDIR /tmrnd_assessment_script

ENV FLASK_APP=app.py

ENV FLASK_RUN_HOST=0.0.0.0

EXPOSE 5000

COPY . .

RUN pip install pandas watchdog flask flask_restful

ENTRYPOINT ["flask", "run"]
