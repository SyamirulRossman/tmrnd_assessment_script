# TMR&D PYTHON ASSESMENT

Hello there!

This is the repository for the Technical Assesment Assignment a fulfillment to the Job Interview at TM Research & Development.
Please read before downloading and executing the script.
If there any inquiries, feel free to contact me at syamirulrossman@gmail.com

Cheers!
---

## Assumptions for this project/assignment
1. The user will copy all the csv files to the files folder (where the csv_loader.py is located)
2. The task_assignment_module is a different script from the main script so that if needed, other script can import the module.
3. task_assignment_module will create assignment_result.json file for the rest_service usage.
4. The client that will be using client to invoke the back-end service to see the tasks.

- Developed in Python Language

---

**Before running the script, please do those things below**

---

## Script in the repository

1. app.py (Client and back-end service) (MAIN)
2. csv_loader.py (script to import data from csv files to the database) 
3. task_assignment_module.py (the module for assigning tasks to teams according to the teams' skills)
4. Dockerfile (for docker/container purposes)
5. docker-compose.yaml (docker configuration)

---

## Running the script (ON HOST)
1. Download the repository.
2. Run app.py
3. The app will automatically process the CSV files in the files folder
4. (Optional) Copy new CSV files to the files folder, the script will process it
5. Go to http://127.0.0.1:5000/ to use the client service.
6. Enter Name and Team then hit submit, the back-end will process the data and return back the tasks assigned to the person.

## Deploying on DOCKER
1. Download the repository.
2. Open Command Prompt or Windows PowerShell, go to the downloaded repository directory.
3. Enter docker-compose up.
4. Go to http://127.0.0.1:5000/ to use the client service.
5. Enter Name and Team then hit submit, the back-end will process the data and return back the tasks assigned to the person.
