#import libraries
import sys
import os
import time
import logging
import threading
import pandas as pd

from datetime import datetime 

import sqlite3

#watchdog library for activity monitoring
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler


#create blank data frame
dataTask = pd.DataFrame()
dataTeam = pd.DataFrame()
dataTeamSkill = pd.DataFrame()

#reading task.csv
def readTask(stop, eventTime):
    while True:
        global dataTask
        dataTask = pd.read_csv("./files/task.csv")

        print (eventTime + " - Reading from : task.csv")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Reading from : task.csv")

        if stop():
            break

#reading team.csv
def readTeam(stop, eventTime):
    while True:
        global dataTeam
        dataTeam = pd.read_csv("./files/team.csv")

        print (eventTime + " - Reading from : team.csv")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Reading from : team.csv")

        if stop():
            break

#reading team_skill.csv
def readTeamSkill(stop, eventTime):
    while True:
        global dataTeamSkill
        dataTeamSkill = pd.read_csv("./files/team_skill.csv")

        print (eventTime + " - Reading from : team_skill.csv")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Reading from : team_skill.csv")

        if stop():
            break

#insert data from task.csv to database
def insertTask(stop, eventTime):
    while True:
        connection = sqlite3.connect("tmrnd.db")

        cursor=connection.cursor()

        #delete the existing data in table
        cursor.execute("DROP TABLE IF EXISTS 'task'")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Inserting contents from task.csv to database")

        print (eventTime + " - Inserting contents from task.csv to database")

        dataTask.to_sql('task', connection, index = False)

        cursor.close()
        connection.close()

        os.remove("./files/task.csv")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - task.csv deleted")

        print (eventTime + " - task.csv deleted")

        if stop():
            break

#insert data from team.csv to database
def insertTeam(stop, eventTime):
    while True:
        connection = sqlite3.connect("tmrnd.db")

        cursor=connection.cursor()

        #delete the existing data in table
        cursor.execute("DROP TABLE IF EXISTS 'team'")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Inserting contents from team.csv to database")

        print (eventTime + " - Inserting contents from team.csv to database")

        dataTeam.to_sql('team', connection, index = False)

        cursor.close()
        connection.close()
        
        os.remove("./files/team.csv")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - team.csv deleted")

        print (eventTime + " - team.csv deleted")

        if stop():
            break

#insert data from team_skill.csv to database
def insertTeamSkill(stop, eventTime):
    while True:
        connection = sqlite3.connect("tmrnd.db")

        cursor=connection.cursor()

        #delete the existing data in table
        cursor.execute("DROP TABLE IF EXISTS 'team_skill'")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Inserting contents from team_skill.csv to database")

        print (eventTime + " - Inserting contents from team_skill.csv to database")

        dataTeamSkill.to_sql('team_skill', connection, index = False)

        cursor.close()
        connection.close()

        os.remove("./files/team_skill.csv")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - team_skill.csv deleted")

        print (eventTime + " - team_skill.csv deleted")
        

        if stop():
            break

#import csv and insert to db when csv file copied initiator
class WatchDogEvent(LoggingEventHandler):
    def on_created(self, event):
        now = datetime.now()
        theEvent = str(event.src_path[8:])
        eventTime = now.strftime("%d/%m/%Y %H:%M:%S")

        if theEvent == "team_skill.csv":

            with open("activity_log.log", "a+") as file_object:
                file_object.seek(0)
                data = file_object.read(100)
                if len(data) > 0 :
                    file_object.write("\n")
                file_object.write(eventTime + " - Found file: team_skill.csv")

            print (eventTime + " - Found file: team_skill.csv")

            stop_threads = False
            t_readTeamSkill = threading.Thread(target=readTeamSkill, daemon=True, args =(lambda : stop_threads, eventTime))
            t_insertTeamSkill = threading.Thread(target=insertTeamSkill, daemon=True, args =(lambda : stop_threads, eventTime))

            t_readTeamSkill.start()
            t_insertTeamSkill.start()

            stop_threads = True
            t_readTeamSkill.join()
            t_insertTeamSkill.join()

        elif theEvent == "task.csv":

            with open("activity_log.log", "a+") as file_object:
                file_object.seek(0)
                data = file_object.read(100)
                if len(data) > 0 :
                    file_object.write("\n")
                file_object.write(eventTime + " - Found file: task.csv")

            print (eventTime + " - Found file: task.csv")

            stop_threads = False
            t_readTask = threading.Thread(target=readTask, daemon=True, args =(lambda : stop_threads, eventTime))
            t_insertTask = threading.Thread(target=insertTask, daemon=True, args =(lambda : stop_threads, eventTime))

            t_readTask.start()
            t_insertTask.start()

            stop_threads = True
            t_readTask.join()
            t_insertTask.join()

        elif theEvent == "team.csv":

            with open("activity_log.log", "a+") as file_object:
                file_object.seek(0)
                data = file_object.read(100)
                if len(data) > 0 :
                    file_object.write("\n")
                file_object.write(eventTime + " - Found file: team.csv")
            
            print (eventTime + " - Found file: team.csv")

            stop_threads = False
            t_readTeam = threading.Thread(target=readTeam, daemon=True, args =(lambda : stop_threads, eventTime))
            t_insertTeam = threading.Thread(target=insertTeam, daemon=True, args =(lambda : stop_threads, eventTime))

            t_readTeam.start()
            t_insertTeam.start()

            stop_threads = True
            t_readTeam.join()
            t_insertTeam.join()

    def on_deleted(self, event):
        now = datetime.now()
        theEvent = str(event.src_path[2:])
        eventTime = now.strftime("%d/%m/%Y %H:%M:%S")

        if theEvent == "task.csv" or theEvent == "team.csv" or theEvent == "team_skill.csv":

            with open("activity_log.log", "a+") as file_object:
                file_object.seek(0)
                data = file_object.read(100)
                if len(data) > 0 :
                    file_object.write("\n")
                file_object.write(eventTime + " - Deleted file: " + theEvent)

            print (eventTime + " - Deleted file: " + theEvent)

            os.system('python task_assignment_module.py')

            with open("activity_log.log", "a+") as file_object:
                file_object.seek(0)
                data = file_object.read(100)
                if len(data) > 0 :
                    file_object.write("\n")
                file_object.write(eventTime + " - assignment_result.json generated related to new " + theEvent)

            print (eventTime + " - assignment_result.json generated related to new " + theEvent)
            


#main

def listenCSV():

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(relativeCreated)6d %(threadName)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    path = "./files"

    observer = Observer()
    observer.schedule(WatchDogEvent(), path, recursive=True)
    observer.start()

    if os.path.isfile('./files/task.csv'):
        now = datetime.now()
        eventTime = now.strftime("%d/%m/%Y %H:%M:%S")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Found file: task.csv")
            
        stop_threads = False
        t_readTeam = threading.Thread(target=readTask, daemon=True, args =(lambda : stop_threads, eventTime))
        t_insertTeam = threading.Thread(target=insertTask, daemon=True, args =(lambda : stop_threads, eventTime))

        t_readTeam.start()
        t_insertTeam.start()

        stop_threads = True
        t_readTeam.join()
        t_insertTeam.join()

    if os.path.isfile('./files/team.csv'):
        now = datetime.now()
        eventTime = now.strftime("%d/%m/%Y %H:%M:%S")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Found file: team.csv")
            
        stop_threads = False
        t_readTeam = threading.Thread(target=readTeam, daemon=True, args =(lambda : stop_threads, eventTime))
        t_insertTeam = threading.Thread(target=insertTeam, daemon=True, args =(lambda : stop_threads, eventTime))

        t_readTeam.start()
        t_insertTeam.start()

        stop_threads = True
        t_readTeam.join()
        t_insertTeam.join()
    
    if os.path.isfile('./files/team_skill.csv'):
        now = datetime.now()
        eventTime = now.strftime("%d/%m/%Y %H:%M:%S")

        with open("activity_log.log", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(100)
            if len(data) > 0 :
                file_object.write("\n")
            file_object.write(eventTime + " - Found file: team_skill.csv")
            
        stop_threads = False
        t_readTeam = threading.Thread(target=readTeamSkill, daemon=True, args =(lambda : stop_threads, eventTime))
        t_insertTeam = threading.Thread(target=insertTeamSkill, daemon=True, args =(lambda : stop_threads, eventTime))

        t_readTeam.start()
        t_insertTeam.start()

        stop_threads = True
        t_readTeam.join()
        t_insertTeam.join()


    try:
        while True:
            time.sleep(1)
    finally:
        observer.stop()
        observer.join()
