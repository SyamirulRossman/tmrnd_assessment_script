#import libraries
import pandas as pd
import sqlite3

#connect to db
try:
    mydb = sqlite3.connect("tmrnd.db")

    sqlTask = "SELECT * FROM task;"
    sqlTeamSkill = "SELECT * FROM team_skill;"

    dataTask = pd.read_sql(sqlTask,mydb)
    dataTeamSkill = pd.read_sql(sqlTeamSkill,mydb)

    mydb.close() #close the connection

except Exception as e:
    mydb.close()
    print(str(e))

#comparing the two dataframe
result = pd.merge(dataTeamSkill,dataTask,on='SKILL',how='outer')

#export result to JSON
result.to_json (r'assignment_result.json')

print ("JSON file created.")